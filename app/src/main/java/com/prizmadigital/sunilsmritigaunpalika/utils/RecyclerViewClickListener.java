package com.prizmadigital.sunilsmritigaunpalika.utils;

import android.view.View;

/**
 * Created by ravi on 3/16/2018.
 */

public interface RecyclerViewClickListener {

    void onClick(View view, int position);
}