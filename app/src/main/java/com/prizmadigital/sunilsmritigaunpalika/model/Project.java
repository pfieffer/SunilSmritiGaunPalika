package com.prizmadigital.sunilsmritigaunpalika.model;

import com.google.gson.annotations.SerializedName;

public class Project {
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("docs")
    private String docUrl;
    @SerializedName("project_category_id")
    private String categoryId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", docUrl='" + docUrl + '\'' +
                ", categoryId='" + categoryId + '\'' +
                '}';
    }
}
