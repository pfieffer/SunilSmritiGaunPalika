package com.prizmadigital.sunilsmritigaunpalika.model;

public class HomeItem {
    private int image;
    private int text;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getText() {
        return text;
    }

    public void setText(int text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "HomeItem{" +
                "image=" + image +
                ", text=" + text +
                '}';
    }
}
