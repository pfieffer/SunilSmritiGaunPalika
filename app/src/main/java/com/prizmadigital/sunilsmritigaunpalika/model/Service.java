package com.prizmadigital.sunilsmritigaunpalika.model;

import com.google.gson.annotations.SerializedName;

public class Service {
    @SerializedName("id")
    private int id;

    @SerializedName("process")
    private String process;

    @SerializedName("office")
    private String serviceProviderOffice;

    @SerializedName("person")
    private String serviceProviderPerson;

    @SerializedName("time")
    private String time;

    @SerializedName("title")
    private String title;

    @SerializedName("required_docs")
    private String requiredDocs;

    @SerializedName("service_category_id")
    private String serviceCategoryId;

    @SerializedName("documents")
    private String documents;

    @SerializedName("more_info")
    private String moreInfo;

    @SerializedName("cost")
    private String cost;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getServiceProviderOffice() {
        return serviceProviderOffice;
    }

    public void setServiceProviderOffice(String serviceProviderOffice) {
        this.serviceProviderOffice = serviceProviderOffice;
    }

    public String getServiceProviderPerson() {
        return serviceProviderPerson;
    }

    public void setServiceProviderPerson(String serviceProviderPerson) {
        this.serviceProviderPerson = serviceProviderPerson;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRequiredDocs() {
        return requiredDocs;
    }

    public void setRequiredDocs(String requiredDocs) {
        this.requiredDocs = requiredDocs;
    }

    public String getServiceCategoryId() {
        return serviceCategoryId;
    }

    public void setServiceCategoryId(String serviceCategoryId) {
        this.serviceCategoryId = serviceCategoryId;
    }

    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(String moreInfo) {
        this.moreInfo = moreInfo;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", process='" + process + '\'' +
                ", serviceProviderOffice='" + serviceProviderOffice + '\'' +
                ", serviceProviderPerson='" + serviceProviderPerson + '\'' +
                ", time='" + time + '\'' +
                ", title='" + title + '\'' +
                ", requiredDocs='" + requiredDocs + '\'' +
                ", serviceCategoryId='" + serviceCategoryId + '\'' +
                ", documents='" + documents + '\'' +
                ", moreInfo='" + moreInfo + '\'' +
                ", cost='" + cost + '\'' +
                '}';
    }
}
