package com.prizmadigital.sunilsmritigaunpalika.model;

public class Law {
    private String title;
    private String docUrl;

    public Law(String title, String docUrl) {
        this.title = title;
        this.docUrl = docUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocUrl() {
        return docUrl;
    }

    public void setDocUrl(String docUrl) {
        this.docUrl = docUrl;
    }

    @Override
    public String toString() {
        return "Law{" +
                "title='" + title + '\'' +
                ", docUrl='" + docUrl + '\'' +
                '}';
    }
}
