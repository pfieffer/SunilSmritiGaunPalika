package com.prizmadigital.sunilsmritigaunpalika.model;

public class OSLibrary {

    private String name;
    private String licenseText;

    public OSLibrary(String name, String licenseText) {
        this.name = name;
        this.licenseText = licenseText;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicenseText() {
        return licenseText;
    }

    public void setLicenseText(String licenseText) {
        this.licenseText = licenseText;
    }
}
