package com.prizmadigital.sunilsmritigaunpalika.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class ImportantPlaceCategory {
    @SerializedName("id")
    private int id;
    @SerializedName("places")
    private ImportantPlace[] places;
    @SerializedName("title")
    private String categoryName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImportantPlace[] getPlaces() {
        return places;
    }

    public void setPlaces(ImportantPlace[] places) {
        this.places = places;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return "ImportantPlaceCategory{" +
                "id=" + id +
                ", places=" + Arrays.toString(places) +
                ", categoryName='" + categoryName + '\'' +
                '}';
    }
}
