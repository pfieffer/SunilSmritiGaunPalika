package com.prizmadigital.sunilsmritigaunpalika.model;

import com.google.gson.annotations.SerializedName;

public class Person {
    @SerializedName("position")
    private String position;
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("email")
    private String email;
    @SerializedName("phone_no")
    private String phoneNumber;

    // the branch field is not required i think
    @SerializedName("branch")
    private String branch;

    @SerializedName("photo")
    private String photo;


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Person{" +
                "position='" + position + '\'' +
                ", id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", branch='" + branch + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}
