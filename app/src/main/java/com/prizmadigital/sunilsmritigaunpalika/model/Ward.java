package com.prizmadigital.sunilsmritigaunpalika.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ward implements Serializable {
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String name;
    @SerializedName("description")
    private String wardDescription;
    @SerializedName("population")
    private String population;
    @SerializedName("docs")
    private String attachment;
    @SerializedName("phone_no")
    private String wardPhoneNumber;
    @SerializedName("electedmember")
    private String electedMember;
    @SerializedName("position")
    private String electedMemberPost;
    @SerializedName("photo")
    private String electedMemberPhoto;
    @SerializedName("mobile")
    private String electedMemberPhoneNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWardDescription() {
        return wardDescription;
    }

    public void setWardDescription(String wardDescription) {
        this.wardDescription = wardDescription;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getWardPhoneNumber() {
        return wardPhoneNumber;
    }

    public void setWardPhoneNumber(String wardPhoneNumber) {
        this.wardPhoneNumber = wardPhoneNumber;
    }

    public String getElectedMember() {
        return electedMember;
    }

    public void setElectedMember(String electedMember) {
        this.electedMember = electedMember;
    }

    public String getElectedMemberPost() {
        return electedMemberPost;
    }

    public void setElectedMemberPost(String electedMemberPost) {
        this.electedMemberPost = electedMemberPost;
    }

    public String getElectedMemberPhoto() {
        return electedMemberPhoto;
    }

    public void setElectedMemberPhoto(String electedMemberPhoto) {
        this.electedMemberPhoto = electedMemberPhoto;
    }

    public String getElectedMemberPhoneNumber() {
        return electedMemberPhoneNumber;
    }

    public void setElectedMemberPhoneNumber(String electedMemberPhoneNumber) {
        this.electedMemberPhoneNumber = electedMemberPhoneNumber;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "Ward{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", wardDescription='" + wardDescription + '\'' +
                ", population='" + population + '\'' +
                ", attachment='" + attachment + '\'' +
                ", wardPhoneNumber='" + wardPhoneNumber + '\'' +
                ", electedMember='" + electedMember + '\'' +
                ", electedMemberPost='" + electedMemberPost + '\'' +
                ", electedMemberPhoto='" + electedMemberPhoto + '\'' +
                ", electedMemberPhoneNumber='" + electedMemberPhoneNumber + '\'' +
                '}';
    }
}
