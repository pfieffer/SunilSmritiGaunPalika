package com.prizmadigital.sunilsmritigaunpalika.model;

import com.google.gson.annotations.SerializedName;

public class Feedback {
    @SerializedName("subject")
    private String subject;
    @SerializedName("category")
    private String category;
    @SerializedName("feedback")
    private String feedback;

    public Feedback(String subject, String category, String feedback) {
        this.subject = subject;
        this.category = category;
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "subject='" + subject + '\'' +
                ", category='" + category + '\'' +
                ", feedback='" + feedback + '\'' +
                '}';
    }
}
