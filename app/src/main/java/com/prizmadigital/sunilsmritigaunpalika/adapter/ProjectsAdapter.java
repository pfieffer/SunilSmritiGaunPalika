package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Project;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ProjectViewHolder> {
    private Context mContext;
    private List<Project> projects;

    //adapter constructor
    public ProjectsAdapter(List<Project> projectList, Context context) {
        this.projects = projectList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_projects, viewGroup, false);
        return new ProjectViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectViewHolder projectViewHolder, int position) {
        final Project project = projects.get(position);

        //setting texts here
        projectViewHolder.titleTV.setText(project.getTitle());
        if (project.getDocUrl() != null) {
            int lastIndexOfSlash = project.getDocUrl().lastIndexOf("/");
            String docName = project.getDocUrl().substring(lastIndexOfSlash + 1);
            //projectViewHolder.docTV.setText(docName);
            projectViewHolder.docTV.setText(Html.fromHtml("<a href=\"" + project.getDocUrl() + "\">" + docName + "</a>"));
            projectViewHolder.docTV.setClickable(true);
            projectViewHolder.docTV.setMovementMethod(LinkMovementMethod.getInstance());

        }

    }

    @Override
    public int getItemCount() {
        return (projects == null ? 0 : projects.size());
    }

    class ProjectViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_project)
        TextView titleTV;
        @BindView(R.id.tv_project_doc)
        TextView docTV;


        private ProjectViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

