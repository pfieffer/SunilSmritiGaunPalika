package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection;
import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Service;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ServiceViewHolder> {

    private final ExpansionLayoutCollection expansionsCollection = new ExpansionLayoutCollection();
    private Context mContext;
    private List<Service> servicesList;

    //adapter constructor
    public ServicesAdapter(List<Service> services, Context context) {
        this.servicesList = services;
        this.mContext = context;
        this.expansionsCollection.openOnlyOne(true);
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_service, viewGroup, false);
        return new ServiceViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder viewHolder, int position) {
        final Service service = servicesList.get(position);

        viewHolder.titleTV.setText(service.getTitle());
        if (service.getTime() != null) {
            viewHolder.timeTV.setText(Html.fromHtml(service.getTime()).toString());
        }
        if (service.getServiceProviderPerson() != null) {
            viewHolder.serviceProviderTV.setText(service.getServiceProviderPerson());
        }
        if (service.getServiceProviderOffice() != null) {
            viewHolder.serviceProviderOfficeTV.setText(service.getServiceProviderOffice());
        }
        if (service.getCost() != null) {
            viewHolder.serviceChargeTV.setText(service.getCost());
        }
        if (service.getDocuments() != null) {
            viewHolder.requiredDocTV.setText(service.getDocuments());
        }
        if (service.getProcess() != null) {
            viewHolder.processTV.setText(Html.fromHtml(service.getProcess()).toString());
        }
        if (service.getMoreInfo() != null) {
            viewHolder.moreInfoTV.setText(Html.fromHtml(service.getMoreInfo()));
        }

        expansionsCollection.add(viewHolder.getExpansionLayout());

    }

    @Override
    public int getItemCount() {
        return (servicesList == null ? 0 : servicesList.size());
    }

    class ServiceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.expansionLayout)
        ExpansionLayout expansionLayout;

        @BindView(R.id.tv_title)
        TextView titleTV;
        @BindView(R.id.tv_time)
        TextView timeTV;
        @BindView(R.id.tv_service_provider)
        TextView serviceProviderTV;
        @BindView(R.id.tv_service_provider_office)
        TextView serviceProviderOfficeTV;
        @BindView(R.id.tv_price)
        TextView serviceChargeTV;

        @BindView(R.id.tv_required_documents)
        TextView requiredDocTV;
        @BindView(R.id.tv_process)
        TextView processTV;
        @BindView(R.id.tv_more_info)
        TextView moreInfoTV;


        private ServiceViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }


        public ExpansionLayout getExpansionLayout() {
            return expansionLayout;
        }
    }
}
