package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.ServiceCategory;
import com.prizmadigital.sunilsmritigaunpalika.utils.RecyclerViewClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceCategoryAdapter extends RecyclerView.Adapter<ServiceCategoryAdapter.ServiceCategoryViewHolder> {

    private Context mContext;
    private List<ServiceCategory> serviceCategoryList;
    private RecyclerViewClickListener mRecyclerItemClickListener;

    public ServiceCategoryAdapter(Context mContext, List<ServiceCategory> serviceCategoryList, RecyclerViewClickListener mRecyclerItemClickListener) {
        this.mContext = mContext;
        this.serviceCategoryList = serviceCategoryList;
        this.mRecyclerItemClickListener = mRecyclerItemClickListener;
    }

    @NonNull
    @Override
    public ServiceCategoryAdapter.ServiceCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_service_category, viewGroup, false);
        return new ServiceCategoryAdapter.ServiceCategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceCategoryAdapter.ServiceCategoryViewHolder serviceCategoryViewHolder, int i) {
        ServiceCategory serviceCategory = serviceCategoryList.get(i);

        serviceCategoryViewHolder.serviceCategoryNameTV.setText(serviceCategory.getTitle());
        serviceCategoryViewHolder.serviceCategoryDescTV.setText(Html.fromHtml(serviceCategory.getDescription()).toString());

    }

    @Override
    public int getItemCount() {
        return (serviceCategoryList == null ? 0 : serviceCategoryList.size());
    }

    public class ServiceCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_service_category_name)
        TextView serviceCategoryNameTV;
        @BindView(R.id.tv_service_category_desc)
        TextView serviceCategoryDescTV;

        public ServiceCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //on click for an item
            mRecyclerItemClickListener.onClick(view, getAdapterPosition());
        }
    }
}
