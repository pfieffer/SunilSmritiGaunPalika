package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.florent37.expansionpanel.ExpansionLayout;
import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection;
import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.FAQ;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.FAQViewHolder> {

    private final static String LOG_TAG = FAQAdapter.class.getSimpleName();

    private final ExpansionLayoutCollection expansionsCollection = new ExpansionLayoutCollection();
    private Context mContext;
    private List<FAQ> faqList;

    //adapter constructor
    public FAQAdapter(List<FAQ> faqs, Context context) {
        this.faqList = faqs;
        this.mContext = context;
        this.expansionsCollection.openOnlyOne(true);
    }

    @NonNull
    @Override
    public FAQViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_faq, viewGroup, false);
        return new FAQViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull FAQViewHolder viewHolder, int position) {
        final FAQ faq = faqList.get(position);

        viewHolder.questionTV.setText(faq.getQuestion());
        viewHolder.answerTV.setText(faq.getAnswer());

        expansionsCollection.add(viewHolder.getExpansionLayout());
    }

    @Override
    public int getItemCount() {
        return (faqList == null ? 0 : faqList.size());
    }

    class FAQViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.faq_expansionLayout)
        ExpansionLayout expansionLayout;

        @BindView(R.id.tv_faq_question)
        TextView questionTV;
        @BindView(R.id.tv_faq_answer)
        TextView answerTV;


        private FAQViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }


        public ExpansionLayout getExpansionLayout() {
            return expansionLayout;
        }
    }
}
