package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.prizmadigital.sunilsmritigaunpalika.fragment.DynamicFragment;
import com.prizmadigital.sunilsmritigaunpalika.model.Ward;

import java.util.List;

public class DynamicFragmentAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private List<Ward> wards;

    public DynamicFragmentAdapter(FragmentManager fm, int NumOfTabs, List<Ward> wardList) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.wards = wardList;
    }

    @Override
    public Fragment getItem(int position) {
        //Put Ward object in bundle and set arguments to the fragments
        Bundle b = new Bundle();
        b.putSerializable("ward", wards.get(position));
        Fragment frag = DynamicFragment.newInstance();
        frag.setArguments(b);
        return frag;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
