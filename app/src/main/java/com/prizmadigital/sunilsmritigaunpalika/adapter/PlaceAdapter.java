package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.ImportantPlace;

import java.util.List;

public class PlaceAdapter extends ArrayAdapter {

    private static final String LOG_TAG = PlaceAdapter.class.getSimpleName();

    private Context mContext;
    private List<ImportantPlace> importantPlaceList;

    public PlaceAdapter(@NonNull Context context, List<ImportantPlace> list) {
        super(context, 0, list);
        mContext = context;
        importantPlaceList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false);

        if (importantPlaceList != null) {
            if (importantPlaceList.size() != 0) {
                ImportantPlace importantPlace = importantPlaceList.get(position);

                TextView textView = listItem.findViewById(R.id.tv_spinner_title);
                textView.setText(importantPlace.getTitle());
            } else {
                Log.e(LOG_TAG, " important place list does not have a single item");
            }
        } else {
            Log.e(LOG_TAG, " importantPlaceList is null");
        }

        return listItem;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false);

        ImportantPlace placeCategory = importantPlaceList.get(position);

        TextView textView = listItem.findViewById(R.id.tv_spinner_title);
        textView.setText(placeCategory.getTitle());

        return listItem;
    }
}