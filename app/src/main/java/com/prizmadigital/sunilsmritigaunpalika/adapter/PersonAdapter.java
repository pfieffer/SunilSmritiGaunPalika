package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Person;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.EmployeeViewHolder> {
    private Context mContext;
    private List<Person> people;

    //adapter constructor
    public PersonAdapter(List<Person> personList, Context context) {
        this.people = personList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_person, viewGroup, false);
        return new EmployeeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder employeeViewHolder, int position) {
        final Person person = people.get(position);

        if (person.getPhoto() != null && !person.getPhoto().equals("")) {
            Picasso.with(mContext).load(person.getPhoto()).into(employeeViewHolder.photoIV);
        }
        //setting texts here
        employeeViewHolder.nameTV.setText(person.getTitle());
        employeeViewHolder.phoneNumberTV.setText(person.getPhoneNumber());
        employeeViewHolder.postTV.setText(person.getPosition());
        employeeViewHolder.emailTV.setText(person.getEmail());
        employeeViewHolder.secondPostTV.setText(person.getBranch());

    }

    @Override
    public int getItemCount() {
        return (people == null ? 0 : people.size());
    }

    class EmployeeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_person_photo)
        ImageView photoIV;
        @BindView(R.id.tv_name)
        TextView nameTV;
        @BindView(R.id.tv_post)
        TextView postTV;
        @BindView(R.id.tv_email)
        TextView emailTV;
        @BindView(R.id.tv_second_post)
        TextView secondPostTV;
        @BindView(R.id.tv_phone_number)
        TextView phoneNumberTV;


        private EmployeeViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
