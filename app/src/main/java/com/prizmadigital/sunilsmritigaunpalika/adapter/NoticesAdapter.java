package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Notice;
import com.prizmadigital.sunilsmritigaunpalika.utils.RecyclerViewClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private Context mContext;
    private List<Notice> noticeList;
    private RecyclerViewClickListener mRecyclerItemClickListener;

    // flag for footer ProgressBar (i.e. last item of list)
    private boolean isLoadingAdded = false;


    public NoticesAdapter(Context mContext, List<Notice> noticeList, RecyclerViewClickListener mRecyclerItemClickListener) {
        this.mContext = mContext;
        this.noticeList = noticeList;
        this.mRecyclerItemClickListener = mRecyclerItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
//        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.item_notice_list, viewGroup, false);
//        return new NoticeViewHolder(v);

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(viewGroup, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new LoadingViewHolder(v2);
                break;
        }
        return viewHolder;

    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.item_notice_list, parent, false);
        viewHolder = new NoticeViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Notice notice = noticeList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                NoticeViewHolder noticeViewHolder = (NoticeViewHolder) holder;

                if (notice.getPhoto() != null && !notice.getPhoto().equals("")) {
                    Picasso.with(mContext).load(notice.getPhoto()).into(noticeViewHolder.noticeHeaderIV);
                }
                noticeViewHolder.titleTV.setText(notice.getTitle());
                noticeViewHolder.dateTV.setText(notice.getDate());
                noticeViewHolder.shortBodyTV.setText(Html.fromHtml(notice.getBody()).toString());
                break;

            case LOADING:
//                Do nothing
                break;
        }


    }

    @Override
    public int getItemCount() {
        return (noticeList == null ? 0 : noticeList.size());
    }

    @Override
    public int getItemViewType(int position) {
        return (position == noticeList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
    Helpers
     */
    public void addLoadingFooter() {
        isLoadingAdded = true;
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
    }


    /*
    ViewHolders
     */
    public class NoticeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_notice_image)
        ImageView noticeHeaderIV;
        @BindView(R.id.tv_notice_title)
        TextView titleTV;
        @BindView(R.id.tv_notice_date)
        TextView dateTV;
        @BindView(R.id.tv_notice_body)
        TextView shortBodyTV;


        private NoticeViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //on click for an item
            mRecyclerItemClickListener.onClick(view, getAdapterPosition());
        }
    }


    protected class LoadingViewHolder extends RecyclerView.ViewHolder {

        public LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}
