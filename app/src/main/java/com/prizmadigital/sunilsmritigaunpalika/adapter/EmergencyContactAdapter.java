package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.florent37.expansionpanel.viewgroup.ExpansionLayoutCollection;
import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.EmergencyContact;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmergencyContactAdapter extends RecyclerView.Adapter<EmergencyContactAdapter.EmergencyContactVH> {

    private final ExpansionLayoutCollection expansionsCollection = new ExpansionLayoutCollection();
    private Context mContext;
    private List<EmergencyContact> emergencyContacts;

    //adapter constructor
    public EmergencyContactAdapter(List<EmergencyContact> contacts, Context context) {
        this.emergencyContacts = contacts;
        this.mContext = context;
        expansionsCollection.openOnlyOne(true);
    }

    @NonNull
    @Override
    public EmergencyContactVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_emegency_contact, viewGroup, false);
        return new EmergencyContactVH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull EmergencyContactVH emergencyContactVH, int position) {
        final EmergencyContact emergencyContact = emergencyContacts.get(position);

        //setting texts here
        emergencyContactVH.nameTV.setText(emergencyContact.getName());
        emergencyContactVH.addressTV.setText(emergencyContact.getAddress());
        emergencyContactVH.phoneTV.setText(emergencyContact.getPhoneNumber());

    }

    @Override
    public int getItemCount() {
        return (emergencyContacts == null ? 0 : emergencyContacts.size());
    }

    class EmergencyContactVH extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_name_emergency_contact)
        TextView nameTV;
        @BindView(R.id.tv_address_emergency_contact)
        TextView addressTV;
        @BindView(R.id.tv_phone_emergency_contact)
        TextView phoneTV;

        private EmergencyContactVH(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }
}
