package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Law;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RuleAndLawsAdapter extends RecyclerView.Adapter<RuleAndLawsAdapter.LawViewHolder> {
    private Context mContext;
    private List<Law> laws;

    //adapter constructor
    public RuleAndLawsAdapter(List<Law> lawList, Context context) {
        this.laws = lawList;
        this.mContext = context;
    }

    @NonNull
    @Override
    public LawViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_projects, viewGroup, false);
        return new LawViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LawViewHolder lawViewHolder, int position) {
        final Law law = laws.get(position);

        //setting texts here
        lawViewHolder.titleTV.setText(law.getTitle());

        if (law.getDocUrl() != null) {
            lawViewHolder.docTV.setText(Html.fromHtml("<a href=\"" + law.getDocUrl() + "\">" + mContext.getResources().getString(R.string.download_document_to_view) + "</a>"));
            lawViewHolder.docTV.setClickable(true);
            lawViewHolder.docTV.setMovementMethod(LinkMovementMethod.getInstance());

        }

    }

    @Override
    public int getItemCount() {
        return (laws == null ? 0 : laws.size());
    }

    class LawViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_project)
        TextView titleTV;
        @BindView(R.id.tv_project_doc)
        TextView docTV;


        private LawViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
