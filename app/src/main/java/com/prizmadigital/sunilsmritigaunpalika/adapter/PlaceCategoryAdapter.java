package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.ImportantPlaceCategory;

import java.util.ArrayList;
import java.util.List;

public class PlaceCategoryAdapter extends ArrayAdapter {

    private Context mContext;
    private List<ImportantPlaceCategory> placeCategories = new ArrayList<>();

    public PlaceCategoryAdapter(@NonNull Context context, ArrayList<ImportantPlaceCategory> list) {
        super(context, 0, list);
        mContext = context;
        placeCategories = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false);

        ImportantPlaceCategory importantPlaceCategory = placeCategories.get(position);

        TextView textView = listItem.findViewById(R.id.tv_spinner_title);
        textView.setText(importantPlaceCategory.getCategoryName());

        return listItem;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_spinner, parent, false);

        ImportantPlaceCategory importantPlaceCategory = placeCategories.get(position);

        TextView textView = listItem.findViewById(R.id.tv_spinner_title);
        textView.setText(importantPlaceCategory.getCategoryName());

        return listItem;
    }

}
