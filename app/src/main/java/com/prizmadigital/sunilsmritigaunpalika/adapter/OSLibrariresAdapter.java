package com.prizmadigital.sunilsmritigaunpalika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.OSLibrary;

import java.util.List;

public class OSLibrariresAdapter extends RecyclerView.Adapter<OSLibrariresAdapter.MyViewHolder> {

    private List<OSLibrary> osLibraries;
    private Context mContext;

    public OSLibrariresAdapter(List<OSLibrary> osLibraries, Context mContext) {
        this.osLibraries = osLibraries;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = View.inflate(parent.getContext(), R.layout.item_open_source_library, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        String name = osLibraries.get(position).getName();
        String text = osLibraries.get(position).getLicenseText();

        holder.osLibraryNameTV.setText(name);
        holder.licenseTV.setText(text);
    }

    @Override
    public int getItemCount() {
        return (osLibraries != null ? osLibraries.size() : 0);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView osLibraryNameTV;
        TextView licenseTV;


        public MyViewHolder(View itemView) {
            super(itemView);
            osLibraryNameTV = itemView.findViewById(R.id.tv_title_library);
            licenseTV = itemView.findViewById(R.id.tv_library_license);
        }
    }
}
