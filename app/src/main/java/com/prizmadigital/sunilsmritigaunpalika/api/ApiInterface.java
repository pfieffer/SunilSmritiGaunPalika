package com.prizmadigital.sunilsmritigaunpalika.api;

import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ContactsResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ImportantPlacesResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.MinimalResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.NoticesResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.PersonResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ProjectsResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ServiceCategoriesResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ServicesResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.SliderResponse;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.WardsResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


/**
 * Created by ravi on 1/28/18.
 */

public interface ApiInterface {

    //get list of contacts
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/contactus/all")
    Call<ContactsResponse> getContacts(
            @Query("offset") int offset
    );

    //get list of notices
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/notice/all")
    Call<NoticesResponse> getNotices(
            @Query("offset") int offset
    );

    //get list of service categories
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/servicecategory/all")
    Call<ServiceCategoriesResponse> getServiceCategories(
    );

    //get list of services associated to a category
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/service/all")
    Call<ServicesResponse> getServices(
            @Query("servicecategory") int categoryId
    );


    //get list of employees of the RMU
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/staffdetail/all")
    Call<PersonResponse> getEmployees(
    );

    //get list of elected members of the RMU
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/electedmember/all")
    Call<PersonResponse> getElectedMembers(
    );

    //get list of wards
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/ward/all")
    Call<WardsResponse> getWards(
            @Query("offset") int offset
    );

    //get list of projects
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/project/all")
    Call<ProjectsResponse> getProjects(
            @Query("offset") int offset,
            @Query("projectcategory") int projectCategory
    );

    //post feedback
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @POST("api/feedback/postfeedback")
    Call<MinimalResponse> postFeedback(
            @Body HashMap<String, String> feedback
    );

    //get important places
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("api/place/all")
    Call<ImportantPlacesResponse> getImportantPlaces(
    );

    @Headers({"Content-type: application/json", "Accept: application/json"})
    @POST("api/problems/register")
    Call<MinimalResponse> registerProblem(
            @Body HashMap<String, String> problem
    );

    //fcm register to our server. Not working
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @POST("api/fcm/register")
    Call<MinimalResponse> registerDeviceId(
            @Body HashMap<String, String> requestBody
    );

    //get slider images
    @Headers({"Content-type: application/json", "Accept: application/json"})
    @GET("/api/slider/all")
    Call<SliderResponse> getSliderPhotos(
    );

}
