package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.OSLibrariresAdapter;
import com.prizmadigital.sunilsmritigaunpalika.model.OSLibrary;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OSCreditActivity extends AppCompatActivity {

    private static final String LOG_TAG = OSCreditActivity.class.getSimpleName();

    @BindView(R.id.toolbar_os_libraries)
    Toolbar toolbar;
    @BindView(R.id.rv_open_source_libraries)
    RecyclerView recyclerView;

    private List<OSLibrary> osLibraryList;
    private OSLibrariresAdapter osLibrariresAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oscredit);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        osLibraryList = new ArrayList<>();
        osLibrariresAdapter = new OSLibrariresAdapter(osLibraryList, getApplicationContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(osLibrariresAdapter);
        for (int i = 0; i < getResources().getStringArray(R.array.os_library_names).length; i++) {
            osLibraryList.add(new OSLibrary(getResources().getStringArray(R.array.os_library_names)[i],
                    getResources().getStringArray(R.array.os_library_license_texts)[i]));
        }
        osLibrariresAdapter.notifyDataSetChanged();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //for back button on the toolbar
                OSCreditActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
