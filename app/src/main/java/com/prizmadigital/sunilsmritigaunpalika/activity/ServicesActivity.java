package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.ServicesAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ServicesResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.Service;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicesActivity extends AppCompatActivity {
    private static final String LOG_TAG = ServicesActivity.class.getSimpleName();
    private static int categoryId;
    @BindView(R.id.toolbar_services)
    Toolbar toolbar;
    @BindView(R.id.rv_services)
    RecyclerView servicesRV;
    @BindView(R.id.progress_services_loading)
    ContentLoadingProgressBar contentLoadingProgressBar;
    @BindView(R.id.tv_no_services_found)
    TextView noServicesFoundTV;
    private List<Service> serviceList;
    private ServicesAdapter servicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("categoryId")) {
            Log.d(LOG_TAG, " Category id got from previous activity: " + getIntent().getIntExtra("categoryId", 0));
            categoryId = getIntent().getIntExtra("categoryId", 1);
        } else {
            Log.e(LOG_TAG, "The categoryId is not present in the intent");
        }

        if (getIntent().hasExtra("categoryName")) {
            setTitle(getIntent().getStringExtra("categoryName"));
        } else {
            Log.e(LOG_TAG, " The categoryName is not present in the intent");
        }

        //setup for toolbar
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        //setup for recycler view
        serviceList = new ArrayList<>();
        servicesAdapter = new ServicesAdapter(serviceList, ServicesActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ServicesActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        servicesRV.setLayoutManager(linearLayoutManager);
        servicesRV.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(ServicesActivity.this, R.dimen.margin_padding_size_small);
        servicesRV.addItemDecoration(itemDecoration);
        servicesRV.setAdapter(servicesAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        getServicesForCategoryId(categoryId);
    }

    private void getServicesForCategoryId(int categoryId) {
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ServicesResponse> call = apiInterface.getServices(categoryId);

        call.enqueue(new Callback<ServicesResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServicesResponse> call, @NonNull Response<ServicesResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
//                        for (Service service : response.body().getServices()) {
//                            Log.d(LOG_TAG, service.toString());
//                        }
                        serviceList.addAll(Arrays.asList(response.body().getServices()));
                        servicesAdapter.notifyDataSetChanged();
                    } else {
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noServicesFoundTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                contentLoadingProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ServicesResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                contentLoadingProgressBar.setVisibility(View.GONE);
                Toast.makeText(ServicesActivity.this, "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //for back button on the toolbar
                ServicesActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
