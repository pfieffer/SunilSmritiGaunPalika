package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.PlaceAdapter;
import com.prizmadigital.sunilsmritigaunpalika.adapter.PlaceCategoryAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ImportantPlacesResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.ImportantPlace;
import com.prizmadigital.sunilsmritigaunpalika.model.ImportantPlaceCategory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImportantPlacesActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener, OnMapReadyCallback {

    private static final String LOG_TAG = ImportantPlacesActivity.class.getSimpleName();
    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    @BindView(R.id.toolbar_important_places)
    Toolbar toolbar;
    @BindView(R.id.spinner_important_place_category)
    AppCompatSpinner categorySpinner;
    @BindView(R.id.spinner_important_place_name)
    AppCompatSpinner importantPlaceNamesSpinner;
    @BindView(R.id.mapView)
    MapView mapView;
    @BindView(R.id.pb_categories_loading)
    ContentLoadingProgressBar contentLoadingProgressBar;
    @BindView(R.id.no_important_places_found)
    TextView noImportantPlacesFoundTV;

    private GoogleMap gmap;
    private String importantPlaceName;
    private double importantPlaceLong;
    private double importantPlaceLat;

    private ArrayList<ImportantPlaceCategory> placeCategories;
    private PlaceCategoryAdapter placeCategoryAdapter;
    private PlaceAdapter placeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_important_places);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        //Get data for spinner from API.
        loadSpinners();


        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
    }

    private void loadSpinners() {
        contentLoadingProgressBar.setVisibility(View.VISIBLE);
        placeCategories = new ArrayList<>();

        placeCategoryAdapter = new PlaceCategoryAdapter(ImportantPlacesActivity.this, placeCategories);
        categorySpinner.setAdapter(placeCategoryAdapter);

        categorySpinner.setOnItemSelectedListener(this);

        getPlaceCategoriesFromAPI();
    }

    private void loadImportantPlacesSpinner(List<ImportantPlace> importantPlaces) {

        placeAdapter = new PlaceAdapter(ImportantPlacesActivity.this, importantPlaces);
        importantPlaceNamesSpinner.setAdapter(placeAdapter);
        importantPlaceNamesSpinner.setOnItemSelectedListener(this);

    }

    private void getPlaceCategoriesFromAPI() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Call<ImportantPlacesResponse> call = apiInterface.getImportantPlaces();

        call.enqueue(new Callback<ImportantPlacesResponse>() {
            @Override
            public void onResponse(@NonNull Call<ImportantPlacesResponse> call, Response<ImportantPlacesResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        placeCategories.addAll(Arrays.asList(response.body().getPlaceCategories()));
                        placeCategoryAdapter.notifyDataSetChanged();
                    } else {
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noImportantPlacesFoundTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                contentLoadingProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NonNull Call<ImportantPlacesResponse> call, Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                contentLoadingProgressBar.setVisibility(View.GONE);
                Toast.makeText(ImportantPlacesActivity.this, "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setMinZoomPreference(13);
        gmap.getUiSettings().setZoomControlsEnabled(true);

        loadGoogleMap();
    }

    private void loadGoogleMap() {
        LatLng latLng = new LatLng(importantPlaceLat, importantPlaceLong);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(importantPlaceName);
        gmap.clear();
        gmap.addMarker(markerOptions);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        gmap.animateCamera(cameraUpdate);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    /*
    Spinner on Item selected callbacks: two callbacks below
     */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        /*
        Because we have two spinners
         */
        switch (adapterView.getId()) {
            case R.id.spinner_important_place_category:
                Log.d(LOG_TAG, " Place category item selected");
                ImportantPlaceCategory selectedCategory = (ImportantPlaceCategory) categorySpinner.getSelectedItem();
                Log.d(LOG_TAG, String.valueOf(selectedCategory.getCategoryName()));
                loadImportantPlacesSpinner(Arrays.asList(selectedCategory.getPlaces()));
                break;
            case R.id.spinner_important_place_name:
                Log.d(LOG_TAG, " Places spinner item selected");
                ImportantPlace selectedImportantPlace = (ImportantPlace) importantPlaceNamesSpinner.getSelectedItem();
                importantPlaceName = selectedImportantPlace.getTitle();
                importantPlaceLat = selectedImportantPlace.getLatitude();
                importantPlaceLong = selectedImportantPlace.getLongitude();
                loadGoogleMap();
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
