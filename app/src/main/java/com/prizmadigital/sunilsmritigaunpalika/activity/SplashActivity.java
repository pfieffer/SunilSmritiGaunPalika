package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.SliderResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.SliderPhoto;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {
    private static final String LOG_TAG = SplashActivity.class.getSimpleName();
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    //TODO: Get slider images from api, make hashmap and send to MainActivity.
    // what happens if no internet?

    private Intent mainIntent;
    private HashMap<String, String> sliderPhotosHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        mainIntent = new Intent(SplashActivity.this, MainActivity.class);

        getSliderPhotosFromApi();
    }

    private void getSliderPhotosFromApi() {
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<SliderResponse> call = apiInterface.getSliderPhotos();

        call.enqueue(new Callback<SliderResponse>() {
            @Override
            public void onResponse(@NonNull Call<SliderResponse> call, @NonNull Response<SliderResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        sliderPhotosHashMap = new HashMap<>();
                        //to hashmap
                        for (SliderPhoto sliderPhoto : response.body().getSliderPhotos()) {
                            sliderPhotosHashMap.put(sliderPhoto.getTitle(), sliderPhoto.getPhoto());
                        }
                        mainIntent.putExtra("sliderPhotoHashMap", sliderPhotosHashMap);
                    } else {
                        Log.e(LOG_TAG, " Response body is null");
                        sliderPhotosHashMap = null;
                    }
                } else {
                    Log.e(LOG_TAG, " Response is not successful");
                    sliderPhotosHashMap = null;
                }

                navigateToMainActivity();
            }

            @Override
            public void onFailure(@NonNull Call<SliderResponse> call, Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                Toast.makeText(SplashActivity.this, "No connection", Toast.LENGTH_SHORT).show();
                sliderPhotosHashMap = null;
                navigateToMainActivity();
            }
        });
    }

    private void navigateToMainActivity() {
        SplashActivity.this.startActivity(mainIntent);
        SplashActivity.this.finish();
    }

}

