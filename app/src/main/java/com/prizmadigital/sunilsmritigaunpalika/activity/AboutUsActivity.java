package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.SmartFragmentStatePagerAdapter;
import com.prizmadigital.sunilsmritigaunpalika.customView.CustomViewPager;
import com.prizmadigital.sunilsmritigaunpalika.fragment.ElectedMembersFragment;
import com.prizmadigital.sunilsmritigaunpalika.fragment.EmployeesFragment;
import com.prizmadigital.sunilsmritigaunpalika.fragment.IntroductionFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.toolbar_about_us)
    Toolbar toolbar;
    @BindView(R.id.tablayout_about_us)
    TabLayout tabLayout;
    @BindView(R.id.view_pager_about_us)
    CustomViewPager viewPager;

    @BindString(R.string.about_us)
    String aboutUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);

        setTitle(aboutUs);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        viewPager.setPagingEnabled(true);
        tabLayout.setupWithViewPager(viewPager);

        CustomPagerAdapter pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());

        //Create fragments here.
        IntroductionFragment introductionFragment = new IntroductionFragment();
        ElectedMembersFragment electedMembersFragment = new ElectedMembersFragment();
        EmployeesFragment employeesFragment = new EmployeesFragment();

        pagerAdapter.addFragments(introductionFragment); //0
        pagerAdapter.addFragments(electedMembersFragment); //1
        pagerAdapter.addFragments(employeesFragment); //2

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        switch (i) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private class CustomPagerAdapter extends SmartFragmentStatePagerAdapter {
        private final List<Fragment> fragments = new ArrayList<>();
        private String tabTitlesAboutUs[] = new String[]{"परिचय", "जनप्रतिनिधी", "कर्मचारीहरु"};

        public CustomPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Our custom method that populates this Adapter with Fragments
        public void addFragments(Fragment fragment) {
            fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitlesAboutUs[position];
        }
    }

}
