package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.MinimalResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {

    private static final String LOG_TAG = FeedbackActivity.class.getSimpleName();

    @BindView(R.id.toolbar_feedback)
    Toolbar toolbar;
    @BindView(R.id.et_feedback_name)
    EditText nameET;
    @BindView(R.id.spinner_feedback_category)
    AppCompatSpinner feedbackCategorySpinner;
    @BindView(R.id.et_feedback_details)
    EditText feedbackDetailsET;
    @BindView(R.id.pb_posting_feedback)
    ProgressBar progressBar;
    @BindView(R.id.btn_submit_feedback)
    Button submitFeedbackButton;

    @BindString(R.string.please_fill_up_this_field)
    String validationText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        //load feedback categories spinner data
        loadFeedbackCategorySpinner();

    }

    private void loadFeedbackCategorySpinner() {
        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("सुझाव");
        spinnerArray.add("उजुरि");
        spinnerArray.add("प्रश्न");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        feedbackCategorySpinner.setAdapter(adapter);
    }

    @OnClick(R.id.btn_submit_feedback)
    void postFeedback() {
        if (validateName() && validateFeedbackDetails()) {
            progressBar.setVisibility(View.VISIBLE);
            submitFeedbackButton.setOnClickListener(null);

            String name = nameET.getText().toString().trim();
//            String categoryNepali = feedbackCategorySpinner.getSelectedItem().toString().trim();
//            String category = getEnglishStringForNepaliCategory(categoryNepali);
            String category = feedbackCategorySpinner.getSelectedItem().toString().trim();
            String details = feedbackDetailsET.getText().toString().trim();

            hitPostFeedbackApi(name, category, details);
        }
    }

//    private String getEnglishStringForNepaliCategory(String categoryNepali) {
//        HashMap<String, String> categoryHM = new HashMap<>();
//        categoryHM.put("सुझाव", "suggestion");
//        categoryHM.put("उजुरि", "sue");
//        categoryHM.put("प्रश्न", "question");
//
//        return  categoryHM.get(categoryNepali);
//    }

    private void hitPostFeedbackApi(String name, String category, String details) {
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        HashMap<String, String> feedback = new HashMap<>();
        feedback.put("subject", name);
        feedback.put("category", category);
        feedback.put("feedback", details);

        Log.d(LOG_TAG, feedback.toString());

        Call<MinimalResponse> call = apiInterface.postFeedback(feedback);

        call.enqueue(new Callback<MinimalResponse>() {
            @Override
            public void onResponse(Call<MinimalResponse> call, Response<MinimalResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            Log.d(LOG_TAG, response.body().getMessage());
                            Toast.makeText(FeedbackActivity.this, "तपाईंको प्रतिक्रिया पेश गरिएको छ। धन्यवाद।", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                            Log.e(LOG_TAG, response.body().getMessage());
                        }
                    } else {
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    Log.e(LOG_TAG, " Response is not successful");
                }

                progressBar.setVisibility(View.INVISIBLE);
                FeedbackActivity.this.finish();
            }

            @Override
            public void onFailure(Call<MinimalResponse> call, Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(FeedbackActivity.this, "No connection", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean validateName() {
        if (nameET.getText().toString().trim().length() > 0) {
            nameET.setError(null);
            return true;
        } else {
            nameET.setError(validationText);
            return false;
        }
    }

    private boolean validateFeedbackDetails() {
        if (feedbackDetailsET.getText().toString().trim().length() > 0) {
            feedbackDetailsET.setError(null);
            return true;
        } else {
            feedbackDetailsET.setError(validationText);
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
