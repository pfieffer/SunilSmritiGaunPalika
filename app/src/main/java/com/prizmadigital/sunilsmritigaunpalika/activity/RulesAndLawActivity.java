package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.RuleAndLawsAdapter;
import com.prizmadigital.sunilsmritigaunpalika.model.Law;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RulesAndLawActivity extends AppCompatActivity {

    private static final String LOG_TAG = RulesAndLawActivity.class.getSimpleName();

    @BindView(R.id.toolbar_rules_and_law)
    Toolbar toolbar;
    @BindView(R.id.rv_rules_and_law)
    RecyclerView recyclerView;

    private List<Law> rulesAndLaws;
    private RuleAndLawsAdapter ruleAndLawsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules_and_law);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        rulesAndLaws = new ArrayList<>();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RulesAndLawActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(RulesAndLawActivity.this, R.dimen.margin_padding_size_small);
        recyclerView.addItemDecoration(itemDecoration);

        ruleAndLawsAdapter = new RuleAndLawsAdapter(rulesAndLaws, RulesAndLawActivity.this);
        recyclerView.setAdapter(ruleAndLawsAdapter);

        //populate list and then notify data set
        rulesAndLaws.add(new Law("1. निति कार्यक्रम तथा बजेट ०७५/७६", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/%E0%A4%A8%E0%A4%BF%E0%A4%A4%E0%A4%BF%20%E0%A4%95%E0%A4%BE%E0%A4%B0%E0%A5%8D%E0%A4%AF%E0%A4%95%E0%A5%8D%E0%A4%B0%E0%A4%AE%20%E0%A4%A4%E0%A4%A5%E0%A4%BE%20%E0%A4%AC%E0%A4%9C%E0%A5%87%E0%A4%9F%E0%A5%A6%E0%A5%AD%E0%A5%AB.%E0%A5%A6%E0%A5%AD%E0%A5%AC.pdf"));
        rulesAndLaws.add(new Law("2. राजस्व परामर्श समितिको ऐन ०७५/७६", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/%E0%A4%B0%E0%A4%BE%E0%A4%9C%E0%A4%B8%E0%A5%8D%E0%A4%B5%20%E0%A4%AA%E0%A4%B0%E0%A4%BE%E0%A4%AE%E0%A4%B0%E0%A5%8D%E0%A4%B6%20%E0%A4%B8%E0%A4%AE%E0%A4%BF%E0%A4%A4%E0%A4%BF%E0%A4%95%E0%A5%8B%20%E0%A4%90%E0%A4%A8.pdf"));
        rulesAndLaws.add(new Law("3. विनियोजन ऐन ०७५/७६", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/%E0%A4%B5%E0%A4%BF%E0%A4%A8%E0%A4%BF%E0%A4%AF%E0%A5%8B%E0%A4%9C%E0%A4%A8%20%E0%A4%90%E0%A4%A8.pdf"));
        rulesAndLaws.add(new Law("4. आर्थिक ऐन ०७५/७६", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/%E0%A4%86%E0%A4%B0%E0%A5%8D%E0%A4%A5%E0%A4%BF%E0%A4%95%20%E0%A4%90%E0%A4%A8.pdf"));

        ruleAndLawsAdapter.notifyDataSetChanged();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
