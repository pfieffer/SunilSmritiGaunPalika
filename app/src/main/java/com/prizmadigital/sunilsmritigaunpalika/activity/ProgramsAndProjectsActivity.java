package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.SmartFragmentStatePagerAdapter;
import com.prizmadigital.sunilsmritigaunpalika.customView.CustomViewPager;
import com.prizmadigital.sunilsmritigaunpalika.fragment.BudgetAndProgramFragment;
import com.prizmadigital.sunilsmritigaunpalika.fragment.PlanAndProjectFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgramsAndProjectsActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    @BindView(R.id.toolbar_programs_and_projects)
    Toolbar toolbar;
    @BindView(R.id.tab_layout_programs_and_projects)
    TabLayout programsAndProjectTabLayout;
    @BindView(R.id.view_pager_programs_and_projects)
    CustomViewPager viewPager;


    @BindString(R.string.programs_and_project)
    String programsAndProjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programs_and_projects);
        ButterKnife.bind(this);

        setTitle(programsAndProjects);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        viewPager.setPagingEnabled(true);
        programsAndProjectTabLayout.setupWithViewPager(viewPager); //this code causing some unusual behavior

        CustomPagerAdapter pagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());

        //Create fragments here.
        BudgetAndProgramFragment budgetAndProgramFragment = new BudgetAndProgramFragment();
        PlanAndProjectFragment planAndProjectFragment = new PlanAndProjectFragment();

        pagerAdapter.addFragments(budgetAndProgramFragment); //0
        pagerAdapter.addFragments(planAndProjectFragment); //1

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private class CustomPagerAdapter extends SmartFragmentStatePagerAdapter {
        private final List<Fragment> fragments = new ArrayList<>();
        private String tabTitlesAboutUs[] = new String[]{"बजेट तथा कार्यक्रम", "योजना तथा परियोजना"};

        public CustomPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Our custom method that populates this Adapter with Fragments
        public void addFragments(Fragment fragment) {
            fragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitlesAboutUs[position];
        }
    }

}
