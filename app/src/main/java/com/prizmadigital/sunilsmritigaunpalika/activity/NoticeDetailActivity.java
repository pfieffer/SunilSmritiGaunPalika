package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Notice;
import com.squareup.picasso.Picasso;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NoticeDetailActivity extends AppCompatActivity {
    private static final String TAG = NoticeDetailActivity.class.getSimpleName();

    @BindView(R.id.toolbar_notice_detail)
    Toolbar toolbar;
    @BindView(R.id.notice_detail_iv)
    ImageView noticeIV;
    @BindView(R.id.tv_notice_detail_date)
    TextView noticeDateTV;
    @BindView(R.id.tv_notice_body_full)
    TextView fullNoticeBodyTV;
    @BindView(R.id.tv_notice_doc)
    TextView noticeDocTV;
    @BindView(R.id.webview)
    WebView webView;

    @BindString(R.string.download_document_to_view)
    String clickToDownload;

    private Notice notice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_detail);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        if (getIntent().hasExtra("notice")) {
            notice = (Notice) getIntent().getSerializableExtra("notice");
            setTitle(notice.getTitle());
            if (notice.getPhoto() != null && !notice.getPhoto().equals("")) {
                Picasso.with(this).load(notice.getPhoto()).into(noticeIV);
            }
            noticeDateTV.setText(notice.getDate());
            fullNoticeBodyTV.setText(Html.fromHtml(notice.getBody()));

            if (!notice.getDocUrl().equals("")) {
                noticeDocTV.setVisibility(View.VISIBLE);
                int lastIndexOfSlash = notice.getDocUrl().lastIndexOf("/");
                String docName = notice.getDocUrl().substring(lastIndexOfSlash + 1);
                noticeDocTV.setText(Html.fromHtml("<a href=\"" + notice.getDocUrl() + "\">" + clickToDownload + "</a>"));
                noticeDocTV.setClickable(true);
                noticeDocTV.setMovementMethod(LinkMovementMethod.getInstance());

                webView.getSettings().setJavaScriptEnabled(true);
                webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + notice.getDocUrl() + "");
            }
        } else {
            Log.d(TAG, " Could not get notice from intent");
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NoticeDetailActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            NoticeDetailActivity.this.finishAfterTransition();
//        }
        NoticeDetailActivity.this.finish();
        super.onBackPressed();
    }
}
