package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.DynamicFragmentAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.WardsResponse;
import com.prizmadigital.sunilsmritigaunpalika.customView.CustomViewPager;
import com.prizmadigital.sunilsmritigaunpalika.model.Ward;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WardActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private static final String LOG_TAG = WardActivity.class.getSimpleName();

    @BindView(R.id.toolbar_ward)
    Toolbar toolbar;
    @BindView(R.id.tablayout_wards)
    TabLayout tabLayout;
    @BindView(R.id.view_pager_wards)
    CustomViewPager viewPager;
    @BindView(R.id.progress_wards_loading)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.tv_no_wards_found)
    TextView noWardsFoundTV;

    private int offset = 0;
    private List<Ward> wards;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ward);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        initViewForTabAndViewPager();
        tabLayout.setVisibility(View.INVISIBLE);
        getWardsFromApi();


    }

    private void initViewForTabAndViewPager() {
        viewPager.setOffscreenPageLimit(5);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getWardsFromApi() {
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<WardsResponse> call = apiInterface.getWards(offset);

        call.enqueue(new Callback<WardsResponse>() {
            @Override
            public void onResponse(@NonNull Call<WardsResponse> call, @NonNull Response<WardsResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    wards = new ArrayList<>();
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            wards.addAll(Arrays.asList(response.body().getWards()));
                            setDynamicFragmentToTabLayout(wards);
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                        }
                    } else {
                        noWardsFoundTV.setVisibility(View.VISIBLE);
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noWardsFoundTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<WardsResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(WardActivity.this, "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setDynamicFragmentToTabLayout(List<Ward> wards) {
        for (int i = 0; i < wards.size(); i++) {
            tabLayout.addTab(tabLayout.newTab().setText(wards.get(i).getName()));
        }
        tabLayout.setVisibility(View.VISIBLE);
        DynamicFragmentAdapter mDynamicFragmentAdapter = new DynamicFragmentAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), wards);
        viewPager.setAdapter(mDynamicFragmentAdapter);
        viewPager.setCurrentItem(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
