package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.EmergencyContactAdapter;
import com.prizmadigital.sunilsmritigaunpalika.model.EmergencyContact;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmergencyNumbersActivity extends AppCompatActivity {

    private static final String LOG_TAG = EmergencyNumbersActivity.class.getSimpleName();

    @BindView(R.id.toolbar_emergency_numbers)
    Toolbar toolbar;
    @BindView(R.id.rv_emergency_numbers)
    RecyclerView emergencyNumbersRV;

    private List<EmergencyContact> emergencyContactList;
    private EmergencyContactAdapter emergencyContactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_numbers);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        //setup for recycler view
        emergencyContactList = new ArrayList<>();
        emergencyContactAdapter = new EmergencyContactAdapter(emergencyContactList, EmergencyNumbersActivity.this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(EmergencyNumbersActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        emergencyNumbersRV.setLayoutManager(linearLayoutManager);
        emergencyNumbersRV.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(EmergencyNumbersActivity.this, R.dimen.margin_padding_size_small);
        emergencyNumbersRV.addItemDecoration(itemDecoration);
        emergencyNumbersRV.setAdapter(emergencyContactAdapter);

        emergencyContactList.add(new EmergencyContact("प्राथमिक स्वास्थ्य केन्द्र", "सुलिचौर, रोल्पा", "+९७७-०८६-४०१०६६"));
        emergencyContactList.add(new EmergencyContact("गजुल हेल्थ पोष्ट", "गजुल, रोल्पा", "+९७७-०८६-६३०१४०"));
        emergencyContactList.add(new EmergencyContact("इलाका प्रहरी कार्यालय", "सुलिचौर, रोल्पा", "१००"));

        emergencyContactAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
