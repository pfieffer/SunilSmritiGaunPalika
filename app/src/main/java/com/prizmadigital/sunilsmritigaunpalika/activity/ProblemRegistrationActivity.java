package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.MinimalResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProblemRegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = ProblemRegistrationActivity.class.getSimpleName();


    @BindView(R.id.toolbar_problem_registration)
    Toolbar toolbar;
    @BindView(R.id.et_problem_registration_name)
    EditText nameET;
    @BindView(R.id.et_problem_details)
    EditText problemDetailsET;
    @BindView(R.id.spinner_wards)
    AppCompatSpinner wardsSpinner;
    @BindView(R.id.spinner_problem_categories)
    AppCompatSpinner problemCategoriesSpinner;
    @BindView(R.id.btn_submit_problem)
    Button submitProblemButton;
    @BindView(R.id.pb_posting_problem)
    ProgressBar progressBar;

    @BindString(R.string.please_fill_up_this_field)
    String validationText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_registration);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        //set onclick listeners
        submitProblemButton.setOnClickListener(this);

        //load spinner data
        loadWardSpinnerData();
        loadProblemCategoriesSpinnerData();

    }

    private void loadProblemCategoriesSpinnerData() {
        // you need to have a list of data that you want the spinner to display
        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("सडक");
        spinnerArray.add("पानी");
        spinnerArray.add("बिजुली");
        spinnerArray.add("शिक्षा");
        spinnerArray.add("अन्य");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        problemCategoriesSpinner.setAdapter(adapter);
    }

    private void loadWardSpinnerData() {
        // you need to have a list of data that you want the spinner to display
        List<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("वडा नं १");
        spinnerArray.add("वडा नं २");
        spinnerArray.add("वडा नं ३");
        spinnerArray.add("वडा नं ४");
        spinnerArray.add("वडा नं ५");
        spinnerArray.add("वडा नं ६");
        spinnerArray.add("वडा नं ७");
        spinnerArray.add("वडा नं ८");
        spinnerArray.add("वडा नं ९");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        wardsSpinner.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_problem:
                Log.d(LOG_TAG, "Submit problem clicked");
                //validate all fields
                if (validateNameET() && validateProblemDetailsET()) {
                    //validated

                    progressBar.setVisibility(View.VISIBLE);
                    submitProblemButton.setOnClickListener(null);

                    String name = nameET.getText().toString().trim();
                    String concernedWard = wardsSpinner.getSelectedItem().toString().trim();
                    String concernedProblemCategory = problemCategoriesSpinner.getSelectedItem().toString().trim();
                    String details = problemDetailsET.getText().toString().trim();

                    hitRegisterProblemApi(name, concernedWard, concernedProblemCategory, details);
                }

                break;
            default:
                Log.d(LOG_TAG, " in default case of onClick()");
                break;
        }
    }

    private void hitRegisterProblemApi(String name, String concernedWard, String concernedProblemCategory, String details) {
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        HashMap<String, String> problem = new HashMap<>();
        problem.put("name", name);
        problem.put("ward", concernedWard);
        problem.put("category", concernedProblemCategory);
        problem.put("description", details);

        Log.d(LOG_TAG, problem.toString());

        Call<MinimalResponse> call = apiInterface.registerProblem(problem);

        call.enqueue(new Callback<MinimalResponse>() {
            @Override
            public void onResponse(Call<MinimalResponse> call, Response<MinimalResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            Log.d(LOG_TAG, response.body().getMessage());
                            Toast.makeText(ProblemRegistrationActivity.this, "तपाईंको प्रतिक्रिया पेश गरिएको छ। धन्यवाद।", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                            Log.e(LOG_TAG, response.body().getMessage());
                        }
                    } else {
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    Log.e(LOG_TAG, " Response is not successful");
                }

                progressBar.setVisibility(View.INVISIBLE);
                ProblemRegistrationActivity.this.finish();
            }

            @Override
            public void onFailure(Call<MinimalResponse> call, Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(ProblemRegistrationActivity.this, "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validateNameET() {
        if (nameET.getText().toString().trim().length() > 0) {
            return true;
        } else {
            nameET.setError(validationText);
            return false;
        }
    }

    private boolean validateProblemDetailsET() {
        if (problemDetailsET.getText().toString().trim().length() > 0) {
            return true;
        } else {
            problemDetailsET.setError(validationText);
            return false;
        }
    }
}
