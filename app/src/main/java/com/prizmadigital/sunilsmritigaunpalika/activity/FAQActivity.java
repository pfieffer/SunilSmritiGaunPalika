package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.FAQAdapter;
import com.prizmadigital.sunilsmritigaunpalika.model.FAQ;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FAQActivity extends AppCompatActivity {

    private static final String LOG_TAG = FAQActivity.class.getSimpleName();

    @BindView(R.id.toolbar_faq)
    Toolbar toolbar;
    @BindView(R.id.rv_faq)
    RecyclerView recyclerView;

    private List<FAQ> faqs;
    private FAQAdapter faqAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        faqs = new ArrayList<>();
        faqAdapter = new FAQAdapter(faqs, FAQActivity.this);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(FAQActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(FAQActivity.this, R.dimen.margin_padding_size_small);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setAdapter(faqAdapter);

        //populate list and then notify data set
        faqs.add(new FAQ("१. नेपाली नागरिकता प्रमाणपत्रको सिफारिस प्राप्त गर्न पेश गर्नुपर्ने कागजातहरु के के हुन ?", "नेपाली नागरिकता प्रमाणपत्रको सिफारिस प्राप्त गर्न पेश गर्नुपर्ने कागजातहरु निम्न छन् |\n" +
                "\n" +
                "१. प्रति नागरिकताको फाराम कालोमसिले भरि दुवै कान देखिने फोटो टाँस गरेको फाराम भरी\n" +
                "२. सनाखत गर्नजाने व्यक्तिको नागरिकता (तीनपुस्ते नाता खोलिएको)\n" +
                "३. जन्म दर्ता\n" +
                "४. शैक्षिक योग्यताको प्रमाण–पत्रविवाह दर्ता, बसाईसराई दर्ता\n" +
                "अस्थायी निस्सा"));
        faqs.add(new FAQ("२. घरजग्गा नामसारी सिफारीस सेवा प्राप्त गर्न पेश गर्नुपर्ने कागजातहरु के के हुन?", "जन्म दर्ता प्रमाणपत्र सेवा प्राप्त गर्न पेश गर्नुपर्ने कागजातहरु निम्न छन् :\n" +
                "\n" +
                "•जन्मको अनुसूची–२ (नियम ५ संग सम्बन्धित) बमोजिमको\n" +
                "•सूचना फाराम (भरेको हुनुपर्ने)\n" +
                "•आमा/बाबुको विवाहदर्ताको प्रतिलिपि\n" +
                "•आमा/बाबुको नेपाली नागरिकताको प्रमाण पत्र प्रतिलिपि\n" +
                "•सूचकको नेपाली नागरिकता प्रमाण पत्रको प्रतिलिपि\n" +
                "•कर्मचारीको छोराछोरीको हकमा सम्बन्धित कार्यालयको\n" +
                "सिफारिस\n" +
                "•आमा-बाबु बसाई सराई भै आएको भए सोको प्रतिलिपि\n" +
                "•अस्पतालमा जन्मिएको भए सोको प्रमाणको प्रतिलिपि\n" +
                "•वडाको किटानी सिफारिस\n" +
                "• पा.को नियमानुसार\n" +
                "•माथि उल्लेखित प्रक्रिया पुरा भएपछि •अनुसूची–१२\n" +
                "अनुसारको प्रमाण–पत्र उपलब्ध गराईनेछ"));
        faqs.add(new FAQ("३. जन्म दर्ता प्रमाणपत्र सेवा प्राप्त गर्न पेश गर्नुपर्ने कागजातहरु के के हुन?", "जन्म दर्ता प्रमाणपत्र सेवा प्राप्त गर्न पेश गर्नुपर्ने कागजातहरु निम्न छन् :\n" +
                "\n" +
                "•जन्मको अनुसूची–२ (नियम ५ संग सम्बन्धित) बमोजिमको\n" +
                "•सूचना फाराम (भरेको हुनुपर्ने)\n" +
                "•आमा/बाबुको विवाहदर्ताको प्रतिलिपि\n" +
                "•आमा/बाबुको नेपाली नागरिकताको प्रमाण पत्र प्रतिलिपि\n" +
                "•सूचकको नेपाली नागरिकता प्रमाण पत्रको प्रतिलिपि\n" +
                "•कर्मचारीको छोराछोरीको हकमा सम्बन्धित कार्यालयको\n" +
                "सिफारिस\n" +
                "•आमा-बाबु बसाई सराई भै आएको भए सोको प्रतिलिपि\n" +
                "•अस्पतालमा जन्मिएको भए सोको प्रमाणको प्रतिलिपि\n" +
                "•वडाको किटानी सिफारिस\n" +
                "• पा.को नियमानुसार\n" +
                "•माथि उल्लेखित प्रक्रिया पुरा भएपछि •अनुसूची–१२\n" +
                "अनुसारको प्रमाण–पत्र उपलब्ध गराईनेछ"));

        faqAdapter.notifyDataSetChanged();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
