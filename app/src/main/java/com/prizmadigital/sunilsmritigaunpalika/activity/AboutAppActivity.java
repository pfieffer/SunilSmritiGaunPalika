package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.BuildConfig;
import com.prizmadigital.sunilsmritigaunpalika.R;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutAppActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = AboutAppActivity.class.getSimpleName();

    @BindView(R.id.toolbar_about_app)
    Toolbar toolbar;
    @BindView(R.id.tv_app_version_code)
    TextView appVersionTV;
    @BindView(R.id.tv_visit_website)
    TextView visitWebsiteTV;
    @BindView(R.id.tv_rate_on_google_play)
    TextView rateOnStoreTV;
    @BindView(R.id.tv_send_email_to_prizma)
    TextView sendEmailTV;
    @BindView(R.id.tv_call_prizma)
    TextView callPrizmaTV;
    @BindView(R.id.tv_os_libraries)
    TextView openSourceTV;

    @BindString(R.string.version)
    String version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_app);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        appVersionTV.setText(String.format(version, BuildConfig.VERSION_NAME));

        visitWebsiteTV.setOnClickListener(this);
        rateOnStoreTV.setOnClickListener(this);
        sendEmailTV.setOnClickListener(this);
        callPrizmaTV.setOnClickListener(this);
        openSourceTV.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //for back button on the toolbar
                AboutAppActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_visit_website:
                openPrizmaWebsite();
                break;
            case R.id.tv_rate_on_google_play:
                openPlayStore();
                break;
            case R.id.tv_send_email_to_prizma:
                sendEmailToPrizma();
                break;
            case R.id.tv_call_prizma:
                callPrizma();
                break;
            case R.id.tv_os_libraries:
                openOSCreditActivity();
                break;
        }
    }

    private void openOSCreditActivity() {
        startActivity(new Intent(AboutAppActivity.this, OSCreditActivity.class));
    }

    private void openPrizmaWebsite() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setData(Uri.parse("https://www.prizmadigital.com"));
        startActivity(browserIntent);
    }

    private void openPlayStore() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.prizmadigital.sunilsmritigaunpalika"));
        startActivity(browserIntent);
    }

    private void callPrizma() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + "+9779845956464"));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }

    private void sendEmailToPrizma() {
        String[] TO = {"mail@prizmadigital.com", "prizmadigital@gmail.com"};

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, TO);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Your Subject");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }
}
