package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.prizmadigital.sunilsmritigaunpalika.R;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements
        DrawerLayout.DrawerListener, View.OnClickListener {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    //navigation drawer
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    //toolbar
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //slider
    @BindView(R.id.homepage_slider)
    SliderLayout sliderLayout;

    //Dashboard items
    @BindView(R.id.tv_select_about_us)
    TextView aboutUs;
    @BindView(R.id.tv_select_notice_and_info)
    TextView noticeAndInfo;
    @BindView(R.id.tv_select_services)
    TextView services;
    @BindView(R.id.tv_select_projects)
    TextView projects;
    @BindView(R.id.tv_select_feedback)
    TextView selectFeedbackTV;
    @BindView(R.id.tv_select_problem_registration)
    TextView problemRegistration;
    @BindView(R.id.tv_select_important_places)
    TextView importantPlaces;
    @BindView(R.id.tv_select_ward_description)
    TextView wardActivity;
    @BindView(R.id.tv_select_emergency_numbers)
    TextView emergencyNumbers;
    @BindView(R.id.tv_about_the_app)
    TextView aboutAppTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupDrawerContent();

        //setup image slider
        setupImageSlider();

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }

        mDrawerLayout.addDrawerListener(this);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);

        //setup click listeners for dashboard items
        setOnClickListenersForDashboardItems();
    }

    private void setOnClickListenersForDashboardItems() {
        aboutUs.setOnClickListener(this);
        services.setOnClickListener(this);
        projects.setOnClickListener(this);
        noticeAndInfo.setOnClickListener(this);
        selectFeedbackTV.setOnClickListener(this);
        problemRegistration.setOnClickListener(this);
        importantPlaces.setOnClickListener(this);
        wardActivity.setOnClickListener(this);
        emergencyNumbers.setOnClickListener(this);
        aboutAppTV.setOnClickListener(this);
    }

    private void setupImageSlider() {
//        HashMap<String, String> sliderImageUrl = new HashMap<>();
//        sliderImageUrl.put("गरिब घरपरिवार परिचय पत्र वितरण कार्यक्रम २०७५/०४/२७", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/IMG%20%287%29_1.JPG");
//        sliderImageUrl.put("'नेपालको संबिधान २०७२' को अवसरमा गरिएको दिपावली कार्यक्रम- संबिधान दिवस २०७५ ", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/Coverpage_1.jpg");
//        sliderImageUrl.put("गाउँपालिका अध्यक्ष गुणेन्द्र घर्तीद्वारा विद्यालय अनुगमन तथा निरीक्षण गर्दै", "http://suwarnawatimun.gov.np/sites/suwarnawatimun.gov.np/files/coverpage%2001.jpg");
        if (getIntent().hasExtra("sliderPhotoHashMap")) {
            HashMap hashMap = new HashMap();
            hashMap = (HashMap) getIntent().getSerializableExtra("sliderPhotoHashMap");

//            for (Object name : hashMap.keySet()) {
//                TextSliderView textSliderView = new TextSliderView(MainActivity.this);
//                // initialize a SliderLayout
//                textSliderView
//                        .description(name.toString())
//                        .image()
//                        .setScaleType(BaseSliderView.ScaleType.CenterCrop);
//
//                sliderLayout.addSlider(textSliderView);
//            }

            Set mapSet = (Set) hashMap.entrySet();
            Iterator mapIterator = mapSet.iterator();
            while (mapIterator.hasNext()) {
                Map.Entry mapEntry = (Map.Entry) mapIterator.next();
                String keyValue = (String) mapEntry.getKey();
                String value = (String) mapEntry.getValue();
                TextSliderView textSliderView = new TextSliderView(MainActivity.this);
                // initialize a SliderLayout
                textSliderView
                        .description(keyValue)
                        .image(value)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop);

                sliderLayout.addSlider(textSliderView);


            }
        }


    }

    private void setupDrawerContent() {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        mDrawerLayout.closeDrawers();  // CLOSE DRAWER
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    private void selectDrawerItem(MenuItem menuItem) {
        Log.d(LOG_TAG, menuItem.getTitle().toString());
        // Add code here to update the UI based on the item selected
        switch (menuItem.getItemId()) {
            //navigation drawer items
            case R.id.nav_contact:
                startActivity(new Intent(MainActivity.this, ContactActivity.class));
                break;
            case R.id.nav_frequently_asked_questions:
                startActivity(new Intent(MainActivity.this, FAQActivity.class));
                break;
            case R.id.nav_rules_and_law:
                startActivity(new Intent(MainActivity.this, RulesAndLawActivity.class));
                break;

        }
        mDrawerLayout.closeDrawers();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d(LOG_TAG, " Hamburger icon clicked");
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    //navigation drawer callbacks
    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        //super.onDrawerSlide(drawerView, slideOffset);
        float slideX = drawerView.getWidth() * slideOffset;
        LinearLayout content = findViewById(R.id.content_main_activity_linear_layout);
        content.setTranslationX(slideX);
    }

    @Override
    public void onDrawerOpened(@NonNull View view) {
        animateMenuIconToBackIcon();
    }

    @Override
    public void onDrawerClosed(@NonNull View view) {
        animateBackIconToMenuIcon();
    }

    @Override
    public void onDrawerStateChanged(int i) {

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            launchExitConfirmDialog();
        }
    }

    private void launchExitConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.dialog_exit_confirm, null);
        builder.setView(layout);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        TextView yesExitTV = layout.findViewById(R.id.tv_yes_confirm_exit);
        TextView noExitTV = layout.findViewById(R.id.tv_no_confirm_exit);

        yesExitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                MainActivity.super.onBackPressed();
            }
        });

        noExitTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_select_about_us:
                startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                break;
            case R.id.tv_select_notice_and_info:
                startActivity(new Intent(MainActivity.this, NoticeListActivity.class));
                break;
            case R.id.tv_select_projects:
                startActivity(new Intent(MainActivity.this, ProgramsAndProjectsActivity.class));
                break;
            case R.id.tv_select_services:
                startActivity(new Intent(MainActivity.this, ServiceCategoryListActivity.class));
                break;
            case R.id.tv_select_feedback:
                startActivity(new Intent(MainActivity.this, FeedbackActivity.class));
                break;
            case R.id.tv_select_problem_registration:
                startActivity(new Intent(MainActivity.this, ProblemRegistrationActivity.class));
                break;
            case R.id.tv_select_important_places:
                startActivity(new Intent(MainActivity.this, ImportantPlacesActivity.class));
                break;
            case R.id.tv_select_ward_description:
                startActivity(new Intent(MainActivity.this, WardActivity.class));
                break;
            case R.id.tv_select_emergency_numbers:
                startActivity(new Intent(MainActivity.this, EmergencyNumbersActivity.class));
                break;

            case R.id.tv_about_the_app:
                mDrawerLayout.closeDrawers();
                startActivity(new Intent(MainActivity.this, AboutAppActivity.class));
                break;
        }
    }

    private void animateMenuIconToBackIcon() {
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AnimatedVectorDrawable drawable = (AnimatedVectorDrawable) getApplicationContext().getDrawable(R.drawable.ic_menu_to_back_animatable);
                getSupportActionBar().setHomeAsUpIndicator(drawable);
                drawable.start();
            } else {
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            }
        }
    }

    private void animateBackIconToMenuIcon() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        }
    }

}
