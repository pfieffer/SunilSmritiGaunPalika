package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.ServiceCategoryAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ServiceCategoriesResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.ServiceCategory;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;
import com.prizmadigital.sunilsmritigaunpalika.utils.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceCategoryListActivity extends AppCompatActivity {

    private static final String LOG_TAG = ServiceCategoryListActivity.class.getSimpleName();

    //View Binding
    @BindView(R.id.toolbar_service_category_list)
    Toolbar toolbar;
    @BindView(R.id.rv_service_category)
    RecyclerView serviceCategoryRV;
    @BindView(R.id.progress_service_categories_loading)
    ContentLoadingProgressBar contentLoadingProgressBar;

    @BindString(R.string.services)
    String services;

    private List<ServiceCategory> serviceCategoryList;
    private ServiceCategoryAdapter serviceCategoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_category_list);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setTitle(services);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ServiceCategoryListActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        serviceCategoryRV.setLayoutManager(linearLayoutManager);
        serviceCategoryRV.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(ServiceCategoryListActivity.this, R.dimen.margin_padding_size_small);
        serviceCategoryRV.addItemDecoration(itemDecoration);

        serviceCategoryList = new ArrayList<>();
        RecyclerViewClickListener recyclerViewClickListener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                startServiceListActivity(serviceCategoryList.get(position));
            }
        };
        serviceCategoryAdapter = new ServiceCategoryAdapter(ServiceCategoryListActivity.this, serviceCategoryList, recyclerViewClickListener);
        serviceCategoryRV.setAdapter(serviceCategoryAdapter);


        getServiceCategoriesFromAPI();
    }

    private void startServiceListActivity(ServiceCategory serviceCategory) {
        Intent intent = new Intent(ServiceCategoryListActivity.this, ServicesActivity.class);
        intent.putExtra("categoryId", serviceCategory.getId());
        intent.putExtra("categoryName", serviceCategory.getTitle());
        startActivity(intent);

    }

    private void getServiceCategoriesFromAPI() {
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ServiceCategoriesResponse> call = apiInterface.getServiceCategories();

        call.enqueue(new Callback<ServiceCategoriesResponse>() {
            @Override
            public void onResponse(@NonNull Call<ServiceCategoriesResponse> call, @NonNull Response<ServiceCategoriesResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        serviceCategoryList.addAll(Arrays.asList(response.body().getServiceCategories()));
                        serviceCategoryAdapter.notifyDataSetChanged();
                    } else {
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    Log.e(LOG_TAG, " Response is not successful");
                }

                contentLoadingProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<ServiceCategoriesResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                contentLoadingProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(ServiceCategoryListActivity.this, "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //for back button on the toolbar
            case android.R.id.home:
                ServiceCategoryListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
