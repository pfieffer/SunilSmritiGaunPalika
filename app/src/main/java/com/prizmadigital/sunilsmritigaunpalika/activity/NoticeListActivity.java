package com.prizmadigital.sunilsmritigaunpalika.activity;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.NoticesAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.NoticesResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.Notice;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;
import com.prizmadigital.sunilsmritigaunpalika.utils.PaginationScrollListener;
import com.prizmadigital.sunilsmritigaunpalika.utils.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticeListActivity extends AppCompatActivity {

    private static final String LOG_TAG = NoticeListActivity.class.getSimpleName();

    @BindView(R.id.toolbar_notice_list)
    Toolbar toolbar;
    @BindView(R.id.rv_notices)
    RecyclerView noticesRV;
    @BindView(R.id.progress_notices_loading)
    ContentLoadingProgressBar contentLoadingProgressBar;
    @BindView(R.id.tv_no_notice_found)
    TextView noNoticeFoundTV;

    @BindString(R.string.notice_and_information)
    String noticeAndInfo;

    private List<Notice> noticeList;
    private NoticesAdapter noticesAdapter;
    private Boolean isLoading = false;
    private Boolean isLastPage = false;
    //    private static final int PAGE_SIZE=10;
    private int offset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_list);
        ButterKnife.bind(this);

        toolbar.setTitleTextColor(Color.WHITE);
        setTitle(noticeAndInfo);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        noticeList = new ArrayList<>();
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(NoticeListActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        noticesRV.setLayoutManager(linearLayoutManager);
        noticesRV.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(NoticeListActivity.this, R.dimen.margin_padding_size_small);
        noticesRV.addItemDecoration(itemDecoration);

        RecyclerViewClickListener recyclerViewClickListener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Toast.makeText(NoticeListActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                startDetailActivity(noticeList.get(position), view);
            }
        };
        noticesAdapter = new NoticesAdapter(NoticeListActivity.this, noticeList, recyclerViewClickListener);
        noticesRV.setAdapter(noticesAdapter);

        getNoticesFromAPi(offset);

//        noticesRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                Log.d(LOG_TAG, " in onScrolled");
//                super.onScrolled(recyclerView, dx, dy);
//                int visibleItemCount = linearLayoutManager.getChildCount();
//                int totalItemCount = linearLayoutManager.getItemCount();
//                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
//
//                Log.d(LOG_TAG, String.valueOf(visibleItemCount));
//                Log.d(LOG_TAG, String.valueOf(totalItemCount));
//                Log.d(LOG_TAG, String.valueOf(firstVisibleItemPosition));
//
//                Log.d(LOG_TAG, String.valueOf(dx));
//                Log.d(LOG_TAG, String.valueOf(dy));
//
//                if (!isLoading && !isLastPage) {
//                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
//                            && firstVisibleItemPosition >= 0
//                            && totalItemCount >= PAGE_SIZE) {
//                        Log.d(LOG_TAG, " HERE I AM!!!");
//
//                        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) noticesRV.getLayoutParams();
//                        layoutParams.bottomMargin= (int) getResources().getDimension(R.dimen.standard_touch_size);//your bottom margin value
//                        noticesRV.setLayoutParams(layoutParams);
//
//                        nextPageLoadingProgressBar.setVisibility(View.VISIBLE);
//                        offset += PAGE_SIZE;
////                        loadMoreItems();
//                        getNoticesFromAPi(offset);
//                        nextPageLoadingProgressBar.setVisibility(View.INVISIBLE);
//                    }
//                }
//            }
//        });

        noticesRV.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isLoading = true;
                        offset += PaginationScrollListener.PAGE_SIZE;
                        noticesAdapter.addLoadingFooter();
                        getNoticesFromAPi(offset);
                        noticesAdapter.removeLoadingFooter();
                    }
                }, 2000);
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void startDetailActivity(Notice notice, View view) {
        Activity container = NoticeListActivity.this;
        Intent intent = new Intent(NoticeListActivity.this, NoticeDetailActivity.class);
        intent.putExtra("notice", notice);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (container != null) {
                container.getWindow().setEnterTransition(new Fade(Fade.IN));
                container.getWindow().setEnterTransition(new Fade(Fade.OUT));
            }
            ActivityOptions options = ActivityOptions
                    .makeSceneTransitionAnimation(container,
                            new Pair<>(view.findViewById(R.id.iv_notice_image),
                                    getString(R.string.transition_drawable)),
                            new Pair<>(view.findViewById(R.id.tv_notice_title),
                                    getString(R.string.transition_message)),
                            new Pair<>(view.findViewById(R.id.tv_notice_body),
                                    getString(R.string.transition_time_and_date)));

            startActivity(intent, options.toBundle());
//            startActivity(intent);

        } else {
            startActivity(intent);
        }

    }

    private void getNoticesFromAPi(int offset) {
        isLoading = true;
        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<NoticesResponse> call = apiInterface.getNotices(offset);

        call.enqueue(new Callback<NoticesResponse>() {
            @Override
            public void onResponse(@NonNull Call<NoticesResponse> call, @NonNull Response<NoticesResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            noticeList.addAll(Arrays.asList(response.body().getNotices()));
                            noticesAdapter.notifyDataSetChanged();
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                            isLastPage = true;
                        }
                    } else {
                        noNoticeFoundTV.setVisibility(View.VISIBLE);
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noNoticeFoundTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                contentLoadingProgressBar.setVisibility(View.INVISIBLE);
                isLoading = false;
            }

            @Override
            public void onFailure(@NonNull Call<NoticesResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                contentLoadingProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(NoticeListActivity.this, "No connection", Toast.LENGTH_SHORT).show();
                isLoading = false;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //for back button on the toolbar
                NoticeListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
