package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.Project;

import java.util.Arrays;

public class ProjectsResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("total")
    private String totalNumberOfProjects;
    @SerializedName("successCode")
    private String successCode;
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private Project[] projects;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalNumberOfProjects() {
        return totalNumberOfProjects;
    }

    public void setTotalNumberOfProjects(String totalNumberOfProjects) {
        this.totalNumberOfProjects = totalNumberOfProjects;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Project[] getProjects() {
        return projects;
    }

    public void setProjects(Project[] projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return "ProjectsResponse{" +
                "message='" + message + '\'' +
                ", totalNumberOfProjects='" + totalNumberOfProjects + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", projects=" + Arrays.toString(projects) +
                '}';
    }
}
