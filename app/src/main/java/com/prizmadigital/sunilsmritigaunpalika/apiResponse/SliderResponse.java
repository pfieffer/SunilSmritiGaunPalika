package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.SliderPhoto;

import java.util.Arrays;

public class SliderResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("successCode")
    private String successCode;
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private SliderPhoto[] sliderPhotos;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SliderPhoto[] getSliderPhotos() {
        return sliderPhotos;
    }

    public void setSliderPhotos(SliderPhoto[] sliderPhotos) {
        this.sliderPhotos = sliderPhotos;
    }

    @Override
    public String toString() {
        return "SliderResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", sliderPhotos=" + Arrays.toString(sliderPhotos) +
                '}';
    }
}
