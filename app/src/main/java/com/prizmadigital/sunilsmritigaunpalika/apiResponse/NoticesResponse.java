package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.Notice;

import java.util.Arrays;

public class NoticesResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("successCode")
    private String successCode;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private Notice[] notices;

    @SerializedName("error")
    private boolean error;

    @SerializedName("code")
    private int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Notice[] getNotices() {
        return notices;
    }

    public void setNotices(Notice[] notices) {
        this.notices = notices;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "NoticesResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", notices=" + Arrays.toString(notices) +
                ", error=" + error +
                ", code=" + code +
                '}';
    }
}
