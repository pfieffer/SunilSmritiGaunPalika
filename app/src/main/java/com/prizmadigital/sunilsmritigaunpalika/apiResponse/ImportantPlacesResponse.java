package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.ImportantPlaceCategory;

import java.util.Arrays;

public class ImportantPlacesResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("successCode")
    private String successCode;
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private ImportantPlaceCategory[] placeCategories;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ImportantPlaceCategory[] getPlaceCategories() {
        return placeCategories;
    }

    public void setPlaceCategories(ImportantPlaceCategory[] placeCategories) {
        this.placeCategories = placeCategories;
    }

    @Override
    public String toString() {
        return "ImportantPlacesResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", placeCategories=" + Arrays.toString(placeCategories) +
                '}';
    }
}
