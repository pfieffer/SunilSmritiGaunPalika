package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.Contact;

import java.util.Arrays;

public class ContactsResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("successCode")
    private int successCode;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private Contact[] contacts;

    @SerializedName("error")
    private boolean error;

    @SerializedName("code")
    private int code;

    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }

    public int getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(int successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Contact[] getContacts() {
        return contacts;
    }

    public void setContacts(Contact[] contacts) {
        this.contacts = contacts;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ContactsResponse{" +
                "message='" + message + '\'' +
                ", successCode=" + successCode +
                ", status='" + status + '\'' +
                ", contacts=" + Arrays.toString(contacts) +
                ", error=" + error +
                ", code=" + code +
                '}';
    }
}
