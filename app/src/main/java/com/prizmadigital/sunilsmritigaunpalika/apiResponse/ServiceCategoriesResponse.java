package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.ServiceCategory;

import java.util.Arrays;

public class ServiceCategoriesResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("successCode")
    private String successCode;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private ServiceCategory[] serviceCategories;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ServiceCategory[] getServiceCategories() {
        return serviceCategories;
    }

    public void setServiceCategories(ServiceCategory[] serviceCategories) {
        this.serviceCategories = serviceCategories;
    }

    @Override
    public String toString() {
        return "ServiceCategoriesResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", serviceCategories=" + Arrays.toString(serviceCategories) +
                '}';
    }
}
