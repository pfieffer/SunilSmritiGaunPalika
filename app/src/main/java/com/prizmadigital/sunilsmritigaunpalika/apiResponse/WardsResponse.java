package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.Ward;

import java.util.Arrays;

public class WardsResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("successCode")
    private String successCode;
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private Ward[] wards;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Ward[] getWards() {
        return wards;
    }

    public void setWards(Ward[] wards) {
        this.wards = wards;
    }

    @Override
    public String toString() {
        return "WardsResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", wards=" + Arrays.toString(wards) +
                '}';
    }
}
