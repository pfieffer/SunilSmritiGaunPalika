package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.Service;

import java.util.Arrays;

public class ServicesResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("successCode")
    private String successCode;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private Service[] services;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Service[] getServices() {
        return services;
    }

    public void setServices(Service[] services) {
        this.services = services;
    }

    @Override
    public String toString() {
        return "ServicesResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", services=" + Arrays.toString(services) +
                '}';
    }
}
