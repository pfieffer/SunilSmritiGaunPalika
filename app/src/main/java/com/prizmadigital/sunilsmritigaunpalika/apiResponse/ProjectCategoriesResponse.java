package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.ProjectCategory;

import java.util.Arrays;

public class ProjectCategoriesResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("successCode")
    private String successCode;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    private ProjectCategory[] projectCategories;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProjectCategory[] getProjectCategories() {
        return projectCategories;
    }

    public void setProjectCategories(ProjectCategory[] projectCategories) {
        this.projectCategories = projectCategories;
    }

    @Override
    public String toString() {
        return "ProjectCategoriesResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", projectCategories=" + Arrays.toString(projectCategories) +
                '}';
    }
}
