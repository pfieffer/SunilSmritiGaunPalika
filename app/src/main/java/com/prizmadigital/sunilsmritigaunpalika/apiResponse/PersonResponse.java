package com.prizmadigital.sunilsmritigaunpalika.apiResponse;

import com.google.gson.annotations.SerializedName;
import com.prizmadigital.sunilsmritigaunpalika.model.Person;

import java.util.Arrays;

public class PersonResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("successCode")
    private String successCode;
    @SerializedName("status")
    private String status;
    @SerializedName("data")
    private Person[] people;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccessCode() {
        return successCode;
    }

    public void setSuccessCode(String successCode) {
        this.successCode = successCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Person[] getPeople() {
        return people;
    }

    public void setPeople(Person[] people) {
        this.people = people;
    }

    @Override
    public String toString() {
        return "PersonResponse{" +
                "message='" + message + '\'' +
                ", successCode='" + successCode + '\'' +
                ", status='" + status + '\'' +
                ", people=" + Arrays.toString(people) +
                '}';
    }
}
