package com.prizmadigital.sunilsmritigaunpalika.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.PersonAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.PersonResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.Person;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ElectedMembersFragment extends Fragment {
    private static final String LOG_TAG = EmployeesFragment.class.getSimpleName();

    @BindView(R.id.rv_elected_members)
    RecyclerView electedMembersRV;
    @BindView(R.id.pb_elected_members_loading)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.tv_no_elected_members_found)
    TextView noElectedMembersFoundTV;

    private List<Person> electedMembers;
    private PersonAdapter personAdapter;

    public ElectedMembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_elected_members, container, false);
        ButterKnife.bind(this, rootView);
        electedMembers = new ArrayList<>();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        electedMembersRV.setLayoutManager(linearLayoutManager);
        electedMembersRV.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.margin_padding_size_small);
        electedMembersRV.addItemDecoration(itemDecoration);


        personAdapter = new PersonAdapter(electedMembers, getContext());
        electedMembersRV.setAdapter(personAdapter);

        getElectedMembersFromApi();

        return rootView;
    }

    private void getElectedMembersFromApi() {
        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<PersonResponse> call = apiInterface.getElectedMembers();

        call.enqueue(new Callback<PersonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PersonResponse> call, @NonNull Response<PersonResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            electedMembers.addAll(Arrays.asList(response.body().getPeople()));
                            personAdapter.notifyDataSetChanged();
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                        }
                    } else {
                        noElectedMembersFoundTV.setVisibility(View.VISIBLE);
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noElectedMembersFoundTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<PersonResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
