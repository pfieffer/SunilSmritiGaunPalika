package com.prizmadigital.sunilsmritigaunpalika.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.PersonAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.PersonResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.Person;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeesFragment extends Fragment {

    private static final String LOG_TAG = EmployeesFragment.class.getSimpleName();

    @BindView(R.id.rv_employees)
    RecyclerView employeesRV;
    @BindView(R.id.pb_employees_loading)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.tv_no_employees_found)
    TextView noEmployeesFoundTV;

    private List<Person> employees;
    private PersonAdapter employeesAdapter;

    public EmployeesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        employees = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_employees, container, false);
        ButterKnife.bind(this, rootView);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        employeesRV.setLayoutManager(linearLayoutManager);
        employeesRV.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.margin_padding_size_small);
        employeesRV.addItemDecoration(itemDecoration);


        employeesAdapter = new PersonAdapter(employees, getContext());
        employeesRV.setAdapter(employeesAdapter);

        getEmployeesFromAPI();

        return rootView;
    }

    private void getEmployeesFromAPI() {
        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<PersonResponse> call = apiInterface.getEmployees();

        call.enqueue(new Callback<PersonResponse>() {
            @Override
            public void onResponse(@NonNull Call<PersonResponse> call, @NonNull Response<PersonResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            employees.addAll(Arrays.asList(response.body().getPeople()));
                            employeesAdapter.notifyDataSetChanged();
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                        }
                    } else {
                        noEmployeesFoundTV.setVisibility(View.VISIBLE);
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noEmployeesFoundTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<PersonResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "No connection", Toast.LENGTH_SHORT).show();
            }
        });

    }
}