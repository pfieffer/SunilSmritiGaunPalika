package com.prizmadigital.sunilsmritigaunpalika.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.model.Ward;
import com.squareup.picasso.Picasso;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DynamicFragment extends Fragment {

    private static final String LOG_TAG = DynamicFragment.class.getSimpleName();

    @BindView(R.id.tv_ward_desc)
    WebView wardDescWV;
    @BindView(R.id.tv_ward_population)
    TextView wardPopulationTV;
    @BindView(R.id.tv_ward_contact_number)
    TextView wardContactTV;

    @BindView(R.id.iv_ward_padaadhikari_photo)
    CircleImageView padaadhikariIV;
    @BindView(R.id.tv_ward_padaadhikari_name)
    TextView padaadhikariNameTV;
    @BindView(R.id.tv_ward_padaadhikari_post)
    TextView padaadkhikariPostTV;
    @BindView(R.id.tv_ward_padaadhikari_phone_number)
    TextView padaadhikariPhonNumberTV;

    @BindString(R.string.ward_contact_number)
    String wardPhoneNumber;
    @BindString(R.string.wardPopulation)
    String wardPopulation;


    public static DynamicFragment newInstance() {
        return new DynamicFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dynamic, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            Ward ward = (Ward) getArguments().getSerializable("ward");
            if (ward != null) {
                initViews(ward);
            } else {
                Log.e(LOG_TAG, " Ward object is null");
            }

        } else {
            Log.e(LOG_TAG, " getArguments() function returned null");
        }

        return view;
    }

    private void initViews(Ward ward) {
        if (ward.getWardDescription() != null) {
            wardDescWV.getSettings().setLoadWithOverviewMode(true);
            wardDescWV.loadData(ward.getWardDescription(), "text/html", "UTF-8");
        } else {
            Log.d(LOG_TAG, " Ward description for " + ward.getName() + " is null");
        }
        if (ward.getWardPhoneNumber() != null) {
            wardContactTV.setText(String.format(wardPhoneNumber, ward.getWardPhoneNumber()));
        } else {
            wardContactTV.setVisibility(View.GONE);
        }
        if (ward.getPopulation() != null) {
            wardPopulationTV.setText(String.format(wardPopulation, ward.getPopulation()));
        } else {
            wardPopulationTV.setVisibility(View.GONE);
        }
        if (ward.getElectedMemberPhoto() != null && !ward.getElectedMemberPhoto().equals("")) {
            Picasso.with(getContext()).load(ward.getElectedMemberPhoto()).into(padaadhikariIV);
        }
        padaadhikariNameTV.setText(ward.getElectedMember());
        padaadkhikariPostTV.setText(ward.getElectedMemberPost());
        padaadhikariPhonNumberTV.setText(ward.getElectedMemberPhoneNumber());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
