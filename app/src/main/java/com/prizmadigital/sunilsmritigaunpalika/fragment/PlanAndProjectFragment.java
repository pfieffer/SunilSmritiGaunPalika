package com.prizmadigital.sunilsmritigaunpalika.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prizmadigital.sunilsmritigaunpalika.R;
import com.prizmadigital.sunilsmritigaunpalika.adapter.ProjectsAdapter;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiClient;
import com.prizmadigital.sunilsmritigaunpalika.api.ApiInterface;
import com.prizmadigital.sunilsmritigaunpalika.apiResponse.ProjectsResponse;
import com.prizmadigital.sunilsmritigaunpalika.model.Project;
import com.prizmadigital.sunilsmritigaunpalika.utils.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlanAndProjectFragment extends Fragment {

    private static final String LOG_TAG = PlanAndProjectFragment.class.getSimpleName();
    @BindView(R.id.rv_plan_and_project)
    RecyclerView recyclerView;
    @BindView(R.id.pb_plan_and_project)
    ContentLoadingProgressBar progressBar;
    @BindView(R.id.tv_no_plan_and_project)
    TextView noPlansAndProjectTV;
    private List<Project> projects;
    private ProjectsAdapter projectsAdapter;

    public PlanAndProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_plan_and_project, container, false);

        ButterKnife.bind(this, rootView);

        projects = new ArrayList<>();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.margin_padding_size_small);
        recyclerView.addItemDecoration(itemDecoration);


        projectsAdapter = new ProjectsAdapter(projects, getContext());
        recyclerView.setAdapter(projectsAdapter);

        getPlansAndProjectsFromApi();

        return rootView;
    }

    private void getPlansAndProjectsFromApi() {
        //category id 4

        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiInterface =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ProjectsResponse> call = apiInterface.getProjects(0, 4);

        call.enqueue(new Callback<ProjectsResponse>() {
            @Override
            public void onResponse(@NonNull Call<ProjectsResponse> call, @NonNull Response<ProjectsResponse> response) {
                Log.d(LOG_TAG, String.valueOf(response.code()));
                Log.d(LOG_TAG, response.toString());

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("SUCCESS")) {
                            projects.addAll(Arrays.asList(response.body().getProjects()));
                            projectsAdapter.notifyDataSetChanged();
                        } else {
                            Log.e(LOG_TAG, response.body().toString());
                        }
                    } else {
                        noPlansAndProjectTV.setVisibility(View.VISIBLE);
                        Log.e(LOG_TAG, " Response body is null");
                    }
                } else {
                    noPlansAndProjectTV.setVisibility(View.VISIBLE);
                    Log.e(LOG_TAG, " Response is not successful");
                }

                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(@NonNull Call<ProjectsResponse> call, @NonNull Throwable t) {
                Log.e(LOG_TAG, " in onFailure() ");
                Log.d(LOG_TAG, t.getLocalizedMessage());
                t.printStackTrace();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "No connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
